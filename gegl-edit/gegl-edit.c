#include <gtk/gtk.h>
#include <gegl.h>
#include <graph-gtk-view.h>
#include <graph-gtk-node.h>
#include "gegl-gtk-property-view.h"
#include <stdlib.h>
#include <string.h>

#include "resources.h"

typedef struct
{
  GtkWidget		*window;

  const gchar		*filename;

  GQueue		*graph_stack;
  GQueue		*view_stack;

  GtkWidget		*view_box;
  GtkWidget		*property_view;

  GraphGtkNode		*context;
  GtkWidget		*popup;
  GtkWidget		*popup2;

  //Node icons
  cairo_surface_t	*in;
  cairo_surface_t	*out;
  cairo_surface_t	*empty;
  cairo_surface_t	*unbounded;

  //About dialog
  GdkPixbuf		*logo;
} CallbackData;

static const gchar* query_proxy(GeglGtkPropertyView *view, GeglNode *node, const gchar *property, CallbackData* data);
static void canvas_rightclicked(GraphGtkView *view, gpointer data);
static void node_selected(GraphGtkView *view, GraphGtkNode *node, gpointer data);
static void node_deselected(GraphGtkView *view, GraphGtkNode *node, gpointer data);
static void node_doubleclicked(GraphGtkView *view, GraphGtkNode *node, gpointer data);
static void node_rightclicked(GraphGtkView *view, GraphGtkNode *node, gpointer data);
static void nodes_connected(GraphGtkView *view, GraphGtkNode *from, const gchar* output, GraphGtkNode *to, const gchar* input, gpointer data);
static void nodes_disconnected(GraphGtkView *view, GraphGtkNode *from, const gchar* output, GraphGtkNode *to, const gchar* input, gpointer data);

static GraphGtkNode* add_gegl_node_to_view(GraphGtkView *view, GeglNode *node, CallbackData *data);
static void load_graph(GraphGtkView *view, GeglNode *node, CallbackData *data);

static void update_images(GraphGtkView *view);
static void update_bg_image(GraphGtkView *view);

static void gegl_node_disconnect_all_pads(GeglNode* node);

//Undocumented gegl functions
GSList		*gegl_node_get_input_pads(GeglNode*);
GSList		*gegl_node_get_pads(GeglNode *self);
const gchar	*gegl_pad_get_name(gpointer);
GeglNode	*gegl_node_get_nth_child (GeglNode*,gint);
const gchar     *gegl_node_get_name (GeglNode *self);
gboolean         gegl_pad_is_output(gpointer *self);
gboolean         gegl_pad_is_input(gpointer *self);

static void
add_ui_to_builder(GResource *resource, GtkBuilder *builder, const gchar* resource_name, CallbackData *data)
{
  GBytes* ui_bytes = g_resource_lookup_data(resource, resource_name, 0, NULL);
  gsize size = g_bytes_get_size(ui_bytes);
  const gchar *ui = g_bytes_get_data(ui_bytes, &size);
  gtk_builder_add_from_string(builder, ui, -1, NULL);
  gtk_builder_connect_signals(builder, data);
  g_bytes_unref(ui_bytes);
}

struct closure
{
  GBytes *bytes;
  unsigned int offset;
};

static cairo_status_t cairo_read_bytes(struct closure *closure, unsigned char *dest, unsigned int length)
{
  gsize size = g_bytes_get_size(closure->bytes);
  const gchar *data = g_bytes_get_data(closure->bytes, &size);

  memcpy(dest, data+closure->offset, length);
  closure->offset += length;

  return CAIRO_STATUS_SUCCESS;
}

static void property_changed(GeglGtkPropertyView *view, gpointer user_data)
{
  update_images(GRAPH_GTK_VIEW(user_data));
}

gint
main (gint	  argc,
      gchar	**argv)
{
  gtk_init(&argc, &argv);
  gegl_init(&argc, &argv);

  GeglNode *gegl = gegl_node_new();
  GResource *resource = gresource_get_resource();

  GtkWidget *window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  GtkWidget *view = graph_gtk_view_new();
  GtkWidget *props = gegl_gtk_property_view_new(NULL);

  CallbackData *data = g_new(CallbackData, 1);
  data->filename = NULL;
  data->window = window;
  data->view_stack = g_queue_new();
  g_queue_push_head(data->view_stack, GRAPH_GTK_VIEW(view));
  data->graph_stack = g_queue_new();
  g_queue_push_head(data->graph_stack, gegl);
  data->property_view = GTK_WIDGET(props);

  //Load images from GResource
  GBytes *in_bytes = g_resource_lookup_data(resource, "/gegl-edit/data/in.png", 0, NULL);
  struct closure closure;
  closure.bytes = in_bytes;
  closure.offset = 0;
  data->in = cairo_image_surface_create_from_png_stream( (cairo_read_func_t) cairo_read_bytes, &closure);
  g_bytes_unref(in_bytes);

  in_bytes = g_resource_lookup_data(resource, "/gegl-edit/data/out.png", 0, NULL);
  closure.bytes = in_bytes;
  closure.offset = 0;
  data->out = cairo_image_surface_create_from_png_stream( (cairo_read_func_t) cairo_read_bytes, &closure);
  g_bytes_unref(in_bytes);

  in_bytes = g_resource_lookup_data(resource, "/gegl-edit/data/empty.png", 0, NULL);
  closure.bytes = in_bytes;
  closure.offset = 0;
  data->empty = cairo_image_surface_create_from_png_stream( (cairo_read_func_t) cairo_read_bytes, &closure);
  g_bytes_unref(in_bytes);

  in_bytes = g_resource_lookup_data(resource, "/gegl-edit/data/unbounded.png", 0, NULL);
  closure.bytes = in_bytes;
  closure.offset = 0;
  data->unbounded = cairo_image_surface_create_from_png_stream( (cairo_read_func_t) cairo_read_bytes, &closure);
  g_bytes_unref(in_bytes);

  in_bytes = g_resource_lookup_data(resource, "/gegl-edit/data/logo.png", 0, NULL);
  gsize logo_size = g_bytes_get_size(in_bytes);
  const gchar *logo_data = g_bytes_get_data(in_bytes, &logo_size);
  data->logo = gdk_pixbuf_new_from_inline(logo_size, logo_data, TRUE, NULL);
  g_bytes_unref(in_bytes);
  //done loading images

  g_signal_connect(props, "property-changed", G_CALLBACK(property_changed), view);
  g_signal_connect(props, "query-proxy", G_CALLBACK(query_proxy), data);

  g_signal_connect(view, "canvas-rightclicked", G_CALLBACK(canvas_rightclicked), data);
  g_signal_connect(view, "node-selected", G_CALLBACK(node_selected), data);
  g_signal_connect(view, "node-deselected", G_CALLBACK(node_deselected), data);
  g_signal_connect(view, "node-doubleclicked", G_CALLBACK(node_doubleclicked), data);
  g_signal_connect(view, "node-rightclicked", G_CALLBACK(node_rightclicked), data);
  g_signal_connect(view, "nodes-connected", G_CALLBACK(nodes_connected), data);
  g_signal_connect(view, "nodes-disconnected", G_CALLBACK(nodes_disconnected), data);

  gtk_window_set_default_size(GTK_WINDOW(window), 700, 500);
  g_signal_connect (window, "destroy", G_CALLBACK( gtk_main_quit), NULL);

  GtkWidget *vbox = gtk_vbox_new(FALSE, 0);
  gtk_container_add(GTK_CONTAINER(window), vbox);

  //Build the menubar from misc/menubar.glade
  GtkBuilder *builder = gtk_builder_new();

  add_ui_to_builder(resource, builder, "/gegl-edit/contextmenu.ui", data);
  GtkMenu *popup = GTK_MENU(gtk_builder_get_object(builder, "context"));
  data->popup = popup;

  add_ui_to_builder(resource, builder, "/gegl-edit/contextmenu_canvas.ui", data);
  GtkMenu *popup2 = GTK_MENU(gtk_builder_get_object(builder, "context_canvas"));
  data->popup2 = popup2;

  add_ui_to_builder(resource, builder, "/gegl-edit/menubar.ui", data);
  
  GtkWidget *menubar = GTK_WIDGET(gtk_builder_get_object(builder, "menu_bar"));

  gtk_box_pack_start(GTK_BOX(vbox), menubar, FALSE, FALSE, 0);

  GtkWidget* pane = gtk_hpaned_new();
  gtk_paned_set_position(GTK_PANED(pane), 550);

  GtkWidget *box = gtk_vbox_new(TRUE, 0);
  gtk_box_pack_start(GTK_BOX(box), view, TRUE, TRUE, 0);
  data->view_box = box;

  gtk_paned_pack1(GTK_PANED(pane), GTK_WIDGET(box), TRUE, TRUE);
  gtk_paned_pack2(GTK_PANED(pane), props, TRUE, TRUE);

  gtk_box_pack_start(GTK_BOX(vbox), pane, TRUE, TRUE, 0);

  gtk_widget_show_all(window);

  gtk_main();
}

////////GraphGtk callbacks////////
static void post_render(GraphGtkNode *view_node, cairo_t *cr, CallbackData *data)
{
  GeglNode *gegl = g_queue_peek_head(data->graph_stack);
  GList *input_proxies = NULL;
  GList *output_proxies = NULL;


  GSList *pads;
  gboolean is_input = FALSE;
  gboolean is_output = FALSE;
  for(pads = gegl_node_get_pads(gegl); pads != NULL; pads = pads->next)
    {
      if(gegl_pad_is_input(pads->data))
	{
	  GeglNode *proxy = gegl_node_get_input_proxy(gegl, gegl_pad_get_name(pads->data));
	  if(view_node->user_data == proxy)
	    is_input = TRUE;
	}
      else if(gegl_pad_is_output(pads->data))
	{
	  GeglNode *proxy = gegl_node_get_output_proxy(gegl, gegl_pad_get_name(pads->data));
	  if(view_node->user_data == proxy)
	    is_output = TRUE;
	}
    }

  gint offset = 0;
  if (is_input)
    {
      cairo_set_source_surface(cr, data->in, offset+view_node->x+view_node->offset_x, view_node->y+view_node->offset_y+view_node->height-cairo_image_surface_get_height(data->in));
      cairo_paint(cr);
      offset += cairo_image_surface_get_width(data->in);
    }
  if (is_output)
    {
      cairo_set_source_surface(cr, data->out, offset+view_node->x+view_node->offset_x, view_node->y+view_node->offset_y+view_node->height-cairo_image_surface_get_height(data->out));
      cairo_paint(cr);
      offset += cairo_image_surface_get_width(data->out);
    }
  
  GeglNode *node = view_node->user_data;
  const GeglRectangle roi = gegl_node_get_bounding_box(node);
  if(gegl_rectangle_is_infinite_plane(&roi))
    {
      cairo_set_source_surface(cr, data->unbounded, offset+view_node->x+view_node->offset_x, view_node->y+view_node->offset_y+view_node->height-cairo_image_surface_get_height(data->unbounded));
      cairo_paint(cr);
      offset += cairo_image_surface_get_width(data->unbounded);
    }
  if(roi.width == 0 || roi.height == 0)
    {
      cairo_set_source_surface(cr, data->empty, offset+view_node->x+view_node->offset_x, view_node->y+view_node->offset_y+view_node->height-cairo_image_surface_get_height(data->empty));
      cairo_paint(cr);
      offset += cairo_image_surface_get_width(data->empty);
    }
}

static const gchar* query_proxy(GeglGtkPropertyView *view, GeglNode *node, const gchar *property, CallbackData* data)
{
  /*  GeglNode *graph = g_queue_peek_head(data->graph_stack);
  if(GEGL_IS_OPERATION_META(node->operation))
    {
      return "meta";
      }*/
  //return "property1";
  return NULL;
}

static void
canvas_rightclicked(GraphGtkView *view, gpointer user_data)
{
  CallbackData *data = (CallbackData*)user_data;
  if(g_queue_get_length(data->graph_stack) > 1)
    {
      gtk_menu_popup(data->popup2, NULL, NULL, NULL, NULL, 3, gtk_get_current_event_time());
    }
}

static void
node_selected(GraphGtkView *view, GraphGtkNode *view_node, gpointer user_data)
{
  CallbackData *data = (CallbackData*)user_data;
  GtkWidget *props = data->property_view;
  gegl_gtk_property_view_set_node(GEGL_GTK_PROPERTY_VIEW(data->property_view), GEGL_NODE(view_node->user_data));
  update_bg_image(view);
}

static void
node_deselected(GraphGtkView *view, GraphGtkNode *node, gpointer user_data)
{
  CallbackData *data = (CallbackData*)user_data;
  GtkWidget *props = data->property_view;
  gegl_gtk_property_view_set_node(GEGL_GTK_PROPERTY_VIEW(data->property_view), NULL);
  graph_gtk_view_set_bg(g_queue_peek_head(data->view_stack), NULL);
}

static void
node_rightclicked(GraphGtkView *view, GraphGtkNode *view_node, gpointer user_data)
{
  CallbackData *data = (CallbackData*)user_data;

  if(GEGL_IS_NODE(view_node->user_data))
    {
      data->context = view_node;
      gtk_menu_popup(data->popup, NULL, NULL, NULL, NULL, 3, gtk_get_current_event_time());
      //open menu
    }
}

static void
node_doubleclicked(GraphGtkView *view, GraphGtkNode *view_node, gpointer data)
{
  if(GEGL_IS_NODE(view_node->user_data))
    {
      GeglNode *node = GEGL_NODE(view_node->user_data);

      if(!view_node->show_image)
	{
	  graph_gtk_node_show_image(view_node, TRUE);
	  update_images(view);
	}
      else
	{
	  graph_gtk_node_show_image(view_node, FALSE);
	}
    }
}

static void
nodes_connected(GraphGtkView *view, GraphGtkNode *from, const gchar* output, GraphGtkNode *to, const gchar* input, gpointer user_data)
{
  CallbackData *data = (CallbackData*)user_data;

  if(GEGL_IS_NODE(from->user_data) && GEGL_IS_NODE(to->user_data))
    {
      gegl_node_connect_to(from->user_data, output, to->user_data, input);

      update_images(view);
    }
}

static void
nodes_disconnected(GraphGtkView *view, GraphGtkNode *from, const gchar* output, GraphGtkNode *to, const gchar* input, gpointer user_data)
{
  if(GEGL_IS_NODE(to->user_data))
    {
      gegl_node_disconnect(to->user_data, input);
      update_images(view);
    }
}


//////////Gtk+ callbacks//////////
G_MODULE_EXPORT void activated_about(GtkMenuItem *menuitem, gpointer user_data)
{
  CallbackData *data = user_data;
  gchar *authors[] = { "Isaac Wagner", NULL };

  gtk_show_about_dialog(GTK_WINDOW(data->window), 
			"copyright", "Copyright (c) 2012 Isaac Wagner 2012 <isaacbwagner@gmail.com>",
			"comments", "Graphical node editor for the GEGL image processing library",
			"license", "GNU LESSER GENERAL PUBLIC LICENSE Version 3, 29 June 2007\n\n(See COPYING)",
			"program-name", "GEGL-edit",
			"version", "0.1",
			"logo", data->logo,
			NULL);
}

G_MODULE_EXPORT void activated_disconnect(GtkMenuItem *menuitem, gpointer user_data)
{
  CallbackData *data = user_data;
  GraphGtkView *view = g_queue_peek_head(data->view_stack);
  gtk_widget_queue_draw(GTK_WIDGET(view));

  if(view->selected_nodes)
    {
      GraphGtkNode *node = view->selected_nodes->data;

      gegl_node_disconnect_all_pads(node->user_data);

      GList *pad;
      for(pad = graph_gtk_node_get_input_pads(node); pad != NULL; pad = pad->next)
	{
	  graph_gtk_pad_disconnect((GraphGtkPad*)(pad->data));
	}

      for(pad = graph_gtk_node_get_output_pads(node); pad != NULL; pad = pad->next)
	{
	  graph_gtk_pad_disconnect((GraphGtkPad*)(pad->data));
	}
    }

  update_images(g_queue_peek_head(data->view_stack));
}

G_MODULE_EXPORT void activated_arrange(GtkMenuItem *menuitem, gpointer user_data)
{
  CallbackData *data = user_data;
  GraphGtkView *view = g_queue_peek_head(data->view_stack);
  graph_gtk_view_arrange(view);
}

G_MODULE_EXPORT void activated_context_return(GtkMenuItem *menuitem, gpointer user_data)
{
  CallbackData *data = user_data;
  GtkWidget *view = GTK_WIDGET(g_queue_pop_head(data->view_stack));
  gtk_container_remove(GTK_CONTAINER(data->view_box), view);
  gtk_box_pack_start(GTK_BOX(data->view_box), GTK_WIDGET(g_queue_peek_head(data->view_stack)), TRUE, TRUE, 0);
  gtk_widget_destroy(view);

  GeglNode *node =   g_queue_pop_head(data->graph_stack);

  GraphGtkNode *view_node = NULL;
  GList *list;
  for(list = GRAPH_GTK_VIEW(g_queue_peek_head(data->view_stack))->nodes; list != NULL; list = list->next)
    {
      GraphGtkNode *n = list->data;
      if(n->user_data == node)
	view_node = n;
    }

  g_assert(view_node);

  //TODO: this won't work if the node is connected to anything
  graph_gtk_node_remove_pads(view_node);
  GSList *pads;
  for(pads = gegl_node_get_pads(node); pads != NULL; pads = pads->next)
    {
      graph_gtk_node_add_pad(view_node, gegl_pad_get_name(pads->data), gegl_pad_is_output(pads->data));
    }
}

G_MODULE_EXPORT void activated_context_edit(GtkMenuItem *menuitem, gpointer user_data)
{
  CallbackData *data = user_data;
  if(data->context)
    {
      GtkWidget *view = graph_gtk_view_new();
      load_graph(GRAPH_GTK_VIEW(view), data->context->user_data, data);

      g_object_ref(g_queue_peek_head(data->view_stack));
      gtk_container_remove(GTK_CONTAINER(data->view_box), g_queue_peek_head(data->view_stack));
      gtk_box_pack_start(GTK_BOX(data->view_box), view, TRUE, TRUE, 0);
      gtk_widget_show(view);

      g_queue_push_head(data->view_stack, view);
      g_queue_push_head(data->graph_stack, data->context->user_data);

      g_signal_connect(view, "canvas-rightclicked", G_CALLBACK(canvas_rightclicked), data);
      g_signal_connect(view, "node-selected", G_CALLBACK(node_selected), data);
      g_signal_connect(view, "node-deselected", G_CALLBACK(node_deselected), data);
      g_signal_connect(view, "node-doubleclicked", G_CALLBACK(node_doubleclicked), data);
      g_signal_connect(view, "node-rightclicked", G_CALLBACK(node_rightclicked), data);
      g_signal_connect(view, "nodes-connected", G_CALLBACK(nodes_connected), data);
      g_signal_connect(view, "nodes-disconnected", G_CALLBACK(nodes_disconnected), data);
    }
}

G_MODULE_EXPORT void activated_context_delete(GtkMenuItem *menuitem, gpointer user_data)
{
  CallbackData *data = user_data;
  gegl_node_disconnect_all_pads(data->context->user_data);
  gegl_node_remove_child(g_queue_peek_head(data->graph_stack), data->context->user_data);
  graph_gtk_view_remove_node(g_queue_peek_head(data->view_stack), data->context);
  update_images(g_queue_peek_head(data->view_stack));
}

G_MODULE_EXPORT void activated_add_subgraph(GtkMenuItem *menuitem, gpointer user_data)
{
  CallbackData *data = user_data;
  GtkWidget* dialog = gtk_dialog_new_with_buttons ("Add Subgraph",
						   GTK_WINDOW(data->window),
						   GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
						   GTK_STOCK_OK, GTK_RESPONSE_ACCEPT,
						   GTK_STOCK_CANCEL, GTK_RESPONSE_REJECT,
						   NULL);

  //gtk_window_set_default_size(GTK_WINDOW(dialog), 350, 80);

  GtkWidget* vbox = gtk_dialog_get_content_area(GTK_DIALOG(dialog));

  GtkWidget* name_label = gtk_label_new("Name");
  GtkWidget* name_entry = gtk_entry_new();
  gtk_box_pack_start(GTK_BOX(vbox), name_label, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), name_entry, FALSE, FALSE, 0);

  gtk_widget_show_all(vbox);

  gint result = gtk_dialog_run(GTK_DIALOG(dialog));

  if(result == GTK_RESPONSE_ACCEPT)
    {
      const gchar *name = gtk_entry_get_text(GTK_ENTRY(name_entry));
      GraphGtkNode *node = graph_gtk_node_new();
      g_signal_connect(node, "post-render", (GCallback)post_render, data);

      graph_gtk_node_set_name(node, name);
      GeglNode *subgraph = gegl_node_new();
      node->user_data = subgraph;
      graph_gtk_view_add_node(g_queue_peek_head(data->view_stack), node);
    }

  gtk_widget_destroy(dialog);
}

G_MODULE_EXPORT void activated_add_input(GtkMenuItem *menuitem, gpointer user_data)
{
  CallbackData *data = user_data;

  GtkWidget* dialog = gtk_dialog_new_with_buttons ("Add Input",
						   GTK_WINDOW(data->window),
						   GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
						   GTK_STOCK_OK, GTK_RESPONSE_ACCEPT,
						   GTK_STOCK_CANCEL, GTK_RESPONSE_REJECT,
						   NULL);

  //gtk_window_set_default_size(GTK_WINDOW(dialog), 350, 80);

  GtkWidget* vbox = gtk_dialog_get_content_area(GTK_DIALOG(dialog));

  GtkWidget* name_label = gtk_label_new("Name");
  GtkWidget* name_entry = gtk_entry_new();
  gtk_entry_set_text(GTK_ENTRY(name_entry), "input");
  gtk_box_pack_start(GTK_BOX(vbox), name_label, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), name_entry, FALSE, FALSE, 0);

  gtk_widget_show_all(vbox);

  gint result = gtk_dialog_run(GTK_DIALOG(dialog));

  if(result == GTK_RESPONSE_ACCEPT)
    {
      const gchar *name = gtk_entry_get_text(GTK_ENTRY(name_entry));
      GraphGtkNode *node = graph_gtk_node_new();
      g_signal_connect(node, "post-render", (GCallback)post_render, data);

      graph_gtk_node_add_pad(node, "output", TRUE);
      graph_gtk_view_add_node(g_queue_peek_head(data->view_stack), node);
      GeglNode *proxy = gegl_node_get_input_proxy(g_queue_peek_head(data->graph_stack), name);
      node->user_data = proxy;
      graph_gtk_node_set_name(node, gegl_node_get_name (proxy));
    }

  gtk_widget_destroy(dialog);
}

G_MODULE_EXPORT void activated_add_output(GtkMenuItem *menuitem, gpointer user_data)
{
  CallbackData *data = user_data;

  GtkWidget* dialog = gtk_dialog_new_with_buttons ("Add Output",
						   GTK_WINDOW(data->window),
						   GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
						   GTK_STOCK_OK, GTK_RESPONSE_ACCEPT,
						   GTK_STOCK_CANCEL, GTK_RESPONSE_REJECT,
						   NULL);

  //gtk_window_set_default_size(GTK_WINDOW(dialog), 350, 80);

  GtkWidget* vbox = gtk_dialog_get_content_area(GTK_DIALOG(dialog));

  GtkWidget* name_label = gtk_label_new("Name");
  GtkWidget* name_entry = gtk_entry_new();
  gtk_entry_set_text(GTK_ENTRY(name_entry), "output");
  gtk_box_pack_start(GTK_BOX(vbox), name_label, FALSE, FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), name_entry, FALSE, FALSE, 0);

  gtk_widget_show_all(vbox);

  gint result = gtk_dialog_run(GTK_DIALOG(dialog));

  if(result == GTK_RESPONSE_ACCEPT)
    {
      const gchar *name = gtk_entry_get_text(GTK_ENTRY(name_entry));
      GraphGtkNode *node = graph_gtk_node_new();
      g_signal_connect(node, "post-render", (GCallback)post_render, data);

      graph_gtk_node_add_pad(node, "input", FALSE);
      graph_gtk_view_add_node(g_queue_peek_head(data->view_stack), node);
      GeglNode *proxy = gegl_node_get_output_proxy(g_queue_peek_head(data->graph_stack), name);
      node->user_data = proxy;
      graph_gtk_node_set_name(node, gegl_node_get_name (proxy));
    }

  gtk_widget_destroy(dialog);
}

G_MODULE_EXPORT void activated_open(GtkMenuItem *menuitem, gpointer user_data)
{
  CallbackData *data = user_data;
  GraphGtkView *view = g_queue_peek_head(data->view_stack);

  GtkFileChooserDialog	*file_select = GTK_FILE_CHOOSER_DIALOG(gtk_file_chooser_dialog_new("Open", GTK_WINDOW(data->window), 
											   GTK_FILE_CHOOSER_ACTION_OPEN, 
											   GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
											   GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
											   NULL));
  gint			 result	     = gtk_dialog_run(GTK_DIALOG(file_select));
  if(result == GTK_RESPONSE_ACCEPT)
    {
      const gchar	*filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(file_select));
      data->filename = filename;
      gtk_window_set_title(GTK_WINDOW(data->window), filename);

      GeglNode *proxy = gegl_node_new_from_file(filename);
      gchar *pad;
      GeglNode *consumer = gegl_node_get_producer(proxy, "input", &pad);
      GeglNode* gegl = gegl_node_new();

      GSList *children;
      for(children = gegl_node_get_children(proxy); children != NULL; children = children->next)
	{
	  GeglNode *child = children->data;
	  g_object_ref(child);
	  gegl_node_remove_child(proxy, child);
	  gegl_node_add_child(gegl, child);
	  g_object_unref(child);
	}

      gegl_node_connect_to(consumer, pad, gegl_node_get_output_proxy(gegl, "output"), "input");

      while(g_queue_get_length(data->graph_stack) > 0)
	{
	  GtkWidget *view = GTK_WIDGET(g_queue_pop_head(data->view_stack));
	  gtk_container_remove(GTK_CONTAINER(data->view_box), view);

	  GeglNode *node =   g_queue_pop_head(data->graph_stack);
	}

      GtkWidget *view = graph_gtk_view_new();
      gtk_box_pack_start(GTK_BOX(data->view_box), view, TRUE, TRUE, 0);
      gtk_widget_show(view);
      load_graph(GRAPH_GTK_VIEW(view), gegl, data);

      g_queue_push_head(data->view_stack, view);
      g_queue_push_head(data->graph_stack, gegl);

      g_signal_connect(view, "canvas-rightclicked", G_CALLBACK(canvas_rightclicked), data);
      g_signal_connect(view, "node-selected", G_CALLBACK(node_selected), data);
      g_signal_connect(view, "node-deselected", G_CALLBACK(node_deselected), data);
      g_signal_connect(view, "node-doubleclicked", G_CALLBACK(node_doubleclicked), data);
      g_signal_connect(view, "node-rightclicked", G_CALLBACK(node_rightclicked), data);
      g_signal_connect(view, "nodes-connected", G_CALLBACK(nodes_connected), data);
      g_signal_connect(view, "nodes-disconnected", G_CALLBACK(nodes_disconnected), data);
    }

  gtk_widget_destroy(GTK_WIDGET(file_select));
}

static GeglNode* getFinalNode(GeglNode* node)
{
  if(gegl_node_find_property(node, "output") == NULL)
    return node;

  GeglNode**	nodes;
  const gchar** pads;
  gint		num_consumers = gegl_node_get_consumers(node, "output", &nodes, &pads);
  
  if(num_consumers == 0)
    return node;
  else
    return getFinalNode(nodes[0]);
}

G_MODULE_EXPORT void activated_save_as(GtkMenuItem *menuitem, gpointer user_data)
{
  CallbackData *data = user_data;
  GtkFileChooserDialog	*file_select = GTK_FILE_CHOOSER_DIALOG(gtk_file_chooser_dialog_new("Save As", GTK_WINDOW(data->window), 
											   GTK_FILE_CHOOSER_ACTION_SAVE, 
											   GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
											   GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT,
											   NULL));
  gint			 result	     = gtk_dialog_run(GTK_DIALOG(file_select));
  if(result == GTK_RESPONSE_ACCEPT)
    {
      const gchar	*filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(file_select));

      gtk_widget_destroy(GTK_WIDGET(file_select));
      GeglNode *last = gegl_node_get_output_proxy(g_queue_peek_head(data->graph_stack), "output");

      gint result = GTK_RESPONSE_OK;

      if(!gegl_node_get_producer(last, "input", NULL))
	{
	  GtkWidget *dialog = gtk_message_dialog_new (GTK_WINDOW(data->window),
						      GTK_DIALOG_DESTROY_WITH_PARENT,
						      GTK_MESSAGE_ERROR,
						      GTK_BUTTONS_OK_CANCEL,
						      "Nothing is connected to the graph's output proxy. Saving will result in an empty graph. Save anyway?");
	  result = gtk_dialog_run (GTK_DIALOG (dialog));
	  gtk_widget_destroy (dialog);
	}

      if(result == GTK_RESPONSE_OK)
	{
	  data->filename = filename;
	  gtk_window_set_title(GTK_WINDOW(data->window), filename);

	  const gchar	*xml = gegl_node_to_xml(last, NULL);

	  g_file_set_contents(data->filename, xml, -1, NULL);	//TODO: check for error
	}
    }
  else
    {
      gtk_widget_destroy(GTK_WIDGET(file_select));
    }
}

G_MODULE_EXPORT void activated_save(GtkMenuItem *menuitem, gpointer user_data)
{
  CallbackData *data = user_data;
  if(data->filename == NULL) {
    activated_save_as(menuitem, user_data);
  }
  else {
    //Try to guess at an output
    //GeglNode* first = gegl_node_get_nth_child(g_queue_peek_head(data->graph_stack), 0);
    //GeglNode* last = getFinalNode(first);
    GeglNode *last = gegl_node_get_output_proxy(g_queue_peek_head(data->graph_stack), "output");

    gint result = GTK_RESPONSE_OK;

    if(!gegl_node_get_producer(last, "input", NULL))
      {
	GtkWidget *dialog = gtk_message_dialog_new (GTK_WINDOW(data->window),
					 GTK_DIALOG_DESTROY_WITH_PARENT,
					 GTK_MESSAGE_ERROR,
					 GTK_BUTTONS_OK_CANCEL,
					 "Nothing is connected to the graph's output proxy. Saving will result in an empty graph. Save anyway?");
	result = gtk_dialog_run (GTK_DIALOG (dialog));
	gtk_widget_destroy (dialog);
      }

    if(result == GTK_RESPONSE_OK)
      {
	const gchar	*xml = gegl_node_to_xml(last, NULL);

	g_file_set_contents(data->filename, xml, -1, NULL);	//TODO: check for error
      }
  }
}

G_MODULE_EXPORT void activated_new(GtkMenuItem *menuitem, gpointer user_data)
{
  CallbackData *data = user_data;
  while(g_queue_get_length(data->graph_stack) > 0)
    {
      GtkWidget *view = GTK_WIDGET(g_queue_pop_head(data->view_stack));
      gtk_container_remove(GTK_CONTAINER(data->view_box), view);
      g_queue_pop_head(data->graph_stack);
    }

  GtkWidget *view = graph_gtk_view_new();
  GeglNode* gegl = gegl_node_new();

  gtk_box_pack_start(GTK_BOX(data->view_box), view, TRUE, TRUE, 0);
  gtk_widget_show(view);
  load_graph(GRAPH_GTK_VIEW(view), gegl, data);

  g_queue_push_head(data->view_stack, view);

  g_queue_push_head(data->graph_stack, gegl);

  g_signal_connect(view, "canvas-rightclicked", G_CALLBACK(canvas_rightclicked), data);
  g_signal_connect(view, "node-selected", G_CALLBACK(node_selected), data);
  g_signal_connect(view, "node-deselected", G_CALLBACK(node_deselected), data);
  g_signal_connect(view, "node-doubleclicked", G_CALLBACK(node_doubleclicked), data);
  g_signal_connect(view, "node-rightclicked", G_CALLBACK(node_rightclicked), data);
  g_signal_connect(view, "nodes-connected", G_CALLBACK(nodes_connected), data);
  g_signal_connect(view, "nodes-disconnected", G_CALLBACK(nodes_disconnected), data);
}

G_MODULE_EXPORT void activated_delete(GtkMenuItem *menuitem, gpointer user_data)
{
  CallbackData *data = user_data;

  GraphGtkView *view = g_queue_peek_head(data->view_stack);
  GeglNode *graph = g_queue_peek_head(data->graph_stack);

  GList *list;
  for(list = view->selected_nodes; list != NULL; list = list->next)
    {
      GraphGtkNode *node = list->data;
      gegl_node_disconnect_all_pads(node->user_data);
      gegl_node_remove_child(graph, node->user_data);
    }
  
  graph_gtk_view_remove_selected_nodes(g_queue_peek_head(data->view_stack));

  update_images(g_queue_peek_head(data->view_stack));
}

/*
G_MODULE_EXPORT void activated_process_selected(GtkMenuItem *menuitem, gpointer user_data)
{
  CallbackData *data = user_data;
  GeglNode *gegl = g_queue_peek_head(data->graph_stack);
  GraphGtkView *view = g_queue_peek_head(data->view_stack);

  GList *nodes;
  for(nodes = view->selected_nodes; nodes != NULL; nodes = nodes->next)
    {
      GraphGtkNode *view_node = (GraphGtkNode*)nodes->data;
      GeglNode *node = (GeglNode*)view_node->user_data;

      g_print("Processing %s\n", gegl_node_get_operation(node));
      gegl_node_process(node);
    }
}

G_MODULE_EXPORT void activated_process_all(GtkMenuItem *menuitem, gpointer user_data)
{
  CallbackData *data = user_data;
  GeglNode *gegl = g_queue_peek_head(data->graph_stack);

  GSList *nodes;
  for(nodes = gegl_node_get_children(gegl); nodes != NULL; nodes = nodes->next)
    {
      GeglNode *node = (GeglNode*)nodes->data;
      gegl_node_process(node);
    }
}
*/

typedef struct {
  GtkWidget *entry;
  GtkListStore *list_store;
  gboolean ignore_change;
} AddDialogData;

void changed_add_entry(GtkEntry *entry, AddDialogData *data) {
  if(!data->ignore_change)
    {
      GtkListStore *store = data->list_store;
      const gchar *str = gtk_entry_get_text(entry);

      gtk_list_store_clear(store);

      guint		n_ops;
      gchar**	ops = gegl_list_operations(&n_ops);

      GtkTreeIter	itr;

      int	i;
      for(i = 0; i < n_ops; i++) {
	if(strstr(ops[i], str))
	  {
	    gtk_list_store_append(store, &itr);
	    gtk_list_store_set(store, &itr, 0, ops[i], -1);
	  }
      }
    }
  else
    {
      data->ignore_change = FALSE;
    }
}

G_MODULE_EXPORT void activated_add(GtkMenuItem *menuitem, gpointer user_data)
{
  CallbackData *data = user_data;

  AddDialogData *callback_data = g_new(AddDialogData, 1);
  callback_data->ignore_change = FALSE;

  GtkWidget	*add_op_dialog = gtk_dialog_new_with_buttons("AddOperation", GTK_WINDOW(data->window), 
							     GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
							     GTK_STOCK_OK, GTK_RESPONSE_ACCEPT, GTK_STOCK_CANCEL,GTK_RESPONSE_REJECT, NULL);
  /////list/////////
  GtkListStore	*store	       = gtk_list_store_new(1, G_TYPE_STRING);
  callback_data->list_store = store;

  guint		n_ops;
  gchar**	ops = gegl_list_operations(&n_ops);

  GtkTreeIter	itr;

  int	i;
  for(i = 0; i < n_ops; i++) {
    gtk_list_store_append(store, &itr);
    gtk_list_store_set(store, &itr, 0, ops[i], -1);
  }

  /////////////////

  GtkWidget *vbox = gtk_vbox_new(FALSE, 0);
  gtk_container_add(GTK_CONTAINER(gtk_dialog_get_content_area(GTK_DIALOG(add_op_dialog))), GTK_WIDGET(vbox));

  GtkWidget	*text_entry = gtk_entry_new();
  callback_data->entry = text_entry;
  //connect to signal which narrows down tree as text is changed
  g_signal_connect(text_entry, "changed", G_CALLBACK(changed_add_entry), callback_data);
  gtk_box_pack_start(GTK_BOX(vbox), text_entry, FALSE, FALSE, 0);
  
  GtkWidget*	list = gtk_tree_view_new_with_model(GTK_TREE_MODEL(store));

  GtkCellRenderer	*renderer;
  GtkTreeViewColumn	*column;

  renderer = gtk_cell_renderer_text_new ();
  column   = gtk_tree_view_column_new_with_attributes ("Operation",
						       renderer,
						       "text", 0,
						       NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW (list), column);

  GtkScrolledWindow*	scrolls = GTK_SCROLLED_WINDOW(gtk_scrolled_window_new(NULL, NULL));
  gtk_widget_set_size_request(GTK_WIDGET(scrolls), 200, 350);
  gtk_widget_show(GTK_WIDGET(scrolls));
  gtk_container_add(GTK_CONTAINER(scrolls), list);

  gtk_box_pack_start(GTK_BOX(vbox), GTK_WIDGET(scrolls), TRUE, TRUE, 0);
  gtk_widget_show_all(vbox);

  //g_signal_connect(add_op_dialog, "response", add_operation_dialog, data);

  gint		 result = gtk_dialog_run(GTK_DIALOG(add_op_dialog));
  if(result == GTK_RESPONSE_ACCEPT)
    {
      GtkTreeSelection	*selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(list));
      GtkTreeModel	*model;
      if(gtk_tree_selection_get_selected(selection, &model, &itr))
	{
	  gchar* operation;
	  gtk_tree_model_get(model, &itr, 0, &operation, -1);
	  GeglNode *node = gegl_node_create_child(g_queue_peek_head(data->graph_stack), operation);

	  add_gegl_node_to_view(g_queue_peek_head(data->view_stack), node, data);
	}
    }

  gtk_widget_destroy(add_op_dialog);
}

static void update_bg_image(GraphGtkView *view)
{
  if(view->selected_nodes)
    {
      if(GEGL_IS_NODE(GRAPH_GTK_NODE(view->selected_nodes->data)->user_data))
	{
	  GeglNode *node = GEGL_NODE(GRAPH_GTK_NODE(view->selected_nodes->data)->user_data);
	  const GeglRectangle roi = gegl_node_get_bounding_box(node);
	  if(!gegl_rectangle_is_infinite_plane(&roi) && roi.width > 0 && roi.height > 0)
	    {
	      const Babl *cairo_argb32 = babl_format("cairo-ARGB32");

	      gdouble scale = 1.0;//150.0/roi.width;
	      GeglRectangle scaled;
	      scaled.x = 0;
	      scaled.y = 0;
	      scaled.width = roi.width*scale;
	      scaled.height = roi.height*scale;

	      gint stride = cairo_format_stride_for_width(CAIRO_FORMAT_ARGB32, scaled.width);
	      guchar* buf = (guchar*)malloc(stride*scaled.height);

	      //make buffer in memory
	      gegl_node_blit(node,
			     scale,
			     &scaled,
			     cairo_argb32,
			     buf,
			     GEGL_AUTO_ROWSTRIDE,
			     GEGL_BLIT_CACHE);

	      cairo_surface_t*	image = 
		cairo_image_surface_create_for_data(buf, CAIRO_FORMAT_ARGB32, 
						    scaled.width, scaled.height, 
						    stride);

	      graph_gtk_view_set_bg(view, image);
	    }
	}
    }
}

static void update_images(GraphGtkView *view)
{
  update_bg_image(view);
  GList *list;
  GraphGtkNode *view_node;
  for(list = view->nodes; list != NULL; list = list->next)
    {
      view_node = GRAPH_GTK_NODE(list->data);

      if(view_node->show_image && GEGL_IS_NODE(view_node->user_data))
	{
	  if(view_node->image)
	    cairo_surface_destroy(view_node->image);

	  GeglNode *node = GEGL_NODE(view_node->user_data);

	  const GeglRectangle roi = gegl_node_get_bounding_box(node);
	  if(gegl_rectangle_is_infinite_plane(&roi))
	    {
	      graph_gtk_node_set_image(view_node, NULL);
	      graph_gtk_node_show_image(view_node, FALSE);
	      continue;
	    }

	  const Babl *cairo_argb32 = babl_format("cairo-ARGB32");

	  gdouble scale = 150.0/roi.width;
	  GeglRectangle scaled;
	  scaled.x = 0;
	  scaled.y = 0;
	  scaled.width = roi.width;
	  scaled.height = roi.height;

	  gint stride = cairo_format_stride_for_width(CAIRO_FORMAT_ARGB32, scaled.width);
	  guchar* buf = (guchar*)malloc(stride*scaled.height);

	  //make buffer in memory
	  gegl_node_blit(node,
			 scale,
			 &scaled,
			 cairo_argb32,
			 buf,
			 GEGL_AUTO_ROWSTRIDE,
			 GEGL_BLIT_CACHE);

	  cairo_surface_t*	image = 
	    cairo_image_surface_create_for_data(buf, CAIRO_FORMAT_ARGB32, 
						scaled.width*scale, scaled.height*scale, 
						stride);

	  graph_gtk_node_set_image_size(view_node, -1, -1);
	  graph_gtk_node_set_image(view_node, image);
	}
    }
}

static GraphGtkNode*
add_gegl_node_to_view(GraphGtkView *view, GeglNode *node, CallbackData *data)
{
  GraphGtkNode *view_node = graph_gtk_node_new();
  g_signal_connect(view_node, "post-render", (GCallback)post_render, data);
  graph_gtk_node_set_name(view_node, gegl_node_get_operation(node));
  view_node->user_data = node;

  GSList *pads;
  for(pads = gegl_node_get_input_pads(node); pads != NULL; pads = pads->next)
    {
      graph_gtk_node_add_pad(view_node, gegl_pad_get_name(pads->data), FALSE);
    }

  if(gegl_node_get_pad(node, "output"))
    graph_gtk_node_add_pad(view_node, "output", TRUE);

  graph_gtk_view_add_node(view, view_node);

  return view_node;
}

static void load_graph(GraphGtkView *view, GeglNode *gegl, CallbackData *data)
{
  GHashTable *hash_table = g_hash_table_new(g_direct_hash, g_direct_equal);

  GList *nodes = NULL;

  GSList *list = gegl_node_get_children(gegl);
  for(;list != NULL; list = list->next)
    {
      GeglNode* node = GEGL_NODE(list->data);
      //g_print("Loading %s\n", gegl_node_get_operation(node));
      GraphGtkNode *view_node = add_gegl_node_to_view(view, node, data);
      const gchar *name = gegl_node_get_name(node);
      if(strcmp(name, "")) //!= ""
	{
	  graph_gtk_node_set_name(view_node,  name);
	}

      g_hash_table_insert(hash_table, node, view_node);
      nodes = g_list_append(nodes, node);
    }

  /*
  GSList *pads;
  for(pads = gegl_node_get_input_pads(gegl); pads != NULL; pads = pads->next)
    {
      GeglNode *node = gegl_node_get_input_proxy(gegl, gegl_pad_get_name(pads->data));
      if(!g_hash_table_lookup(hash_table, node))
	{
	  GraphGtkNode *view_node = add_gegl_node_to_view(view, node, data);

	  graph_gtk_node_set_name(view_node,  gegl_node_get_name (node));

	  g_hash_table_insert(hash_table, node, view_node);
	  nodes = g_list_append(nodes, node);
	}
    }

  if(gegl_node_get_pad(gegl, "output"))
    {
      GeglNode *node = gegl_node_get_output_proxy(gegl, "output");
      if(!g_hash_table_lookup(hash_table, node))
	{
	  GraphGtkNode *view_node = add_gegl_node_to_view(view, node, data);

	  graph_gtk_node_set_name(view_node, gegl_node_get_name (node));

	  g_hash_table_insert(hash_table, node, view_node);
	  nodes = g_list_append(nodes, node);
	}
    }
  */

  //connect nodes
  GList *itr;
  for(itr = nodes; itr != NULL; itr = itr->next)
    {
      GeglNode* node = GEGL_NODE(itr->data);

      GraphGtkNode *from = g_hash_table_lookup(hash_table, node);

      if(!from)
	break;

      if(!gegl_node_has_pad(node, "output")) 
	break;

      GeglNode** nodes;
      const gchar** pads;
      gint num = gegl_node_get_consumers(node, "output", &nodes, &pads);
	  
      int i;
      for(i = 0; i < num; i++)
	{
	  GraphGtkNode *to = g_hash_table_lookup(hash_table, nodes[i]);
	  if(!to)
	    break;

	  graph_gtk_node_connect_to(from, "output", to, pads[i]);
	}
    }

  g_hash_table_destroy(hash_table);
  g_list_free(nodes);

  graph_gtk_view_arrange(view);
}

static void
gegl_node_disconnect_all_pads(GeglNode* node)
{
  GSList* list;
  for(list = gegl_node_get_pads(node); list != NULL; list = list->next)
    {
      if(gegl_pad_is_input(list->data)) //disconnect inputs
	{
	  gegl_node_disconnect(node, (gchar*)gegl_pad_get_name(list->data));
	}
      else if(gegl_pad_is_output(list->data)) //disconnect outputs
	{
	  GeglNode** nodes;
	  const gchar** pads;
	  gint num_consumers = gegl_node_get_consumers(node, gegl_pad_get_name(list->data), &nodes, &pads);
	  gint i;
	  for(i = 0; i < num_consumers; i++)
	    {
	      gegl_node_disconnect(nodes[i], pads[i]);
	    }
	}
    }
}
