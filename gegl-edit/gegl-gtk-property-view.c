#include "gegl-gtk-property-view.h"
#include <gegl.h>
#include <stdlib.h>

static void gegl_gtk_property_view_dispose (GObject *object);
static void gegl_gtk_property_view_finalize (GObject *object);

G_DEFINE_TYPE (GeglGtkPropertyView, gegl_gtk_property_view, GTK_TYPE_BOX);

enum {
  PROP_0,
  PROP_NODE,
  N_PROPERTIES,
};

static GParamSpec *obj_properties[N_PROPERTIES] = { NULL, };

static rebuild_contents(GeglGtkPropertyView *self);

static void
gegl_gtk_property_view_set_property (GObject      *object,
				     guint         property_id,
				     const GValue *value,
				     GParamSpec   *pspec)
{
  GeglGtkPropertyView *self = GEGL_GTK_PROPERTY_VIEW(object);
  switch(property_id)
    {
    case PROP_NODE:
      self->node = g_value_get_pointer(value);
      rebuild_contents(self);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
gegl_gtk_property_view_get_property (GObject      *object,
				     guint         property_id,
				     GValue *value,
				     GParamSpec   *pspec)
{
  GeglGtkPropertyView *self = GEGL_GTK_PROPERTY_VIEW(object);
  switch(property_id)
    {
    case PROP_NODE:
      g_value_set_pointer(value, self->node);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
    }
}

static void
gegl_gtk_property_view_class_init (GeglGtkPropertyViewClass *klass)
{
  GObjectClass *gobject_class = (GObjectClass *)klass;

  gobject_class->set_property = gegl_gtk_property_view_set_property;
  gobject_class->get_property = gegl_gtk_property_view_get_property;
  gobject_class->dispose = gegl_gtk_property_view_dispose;
  gobject_class->finalize = gegl_gtk_property_view_finalize;

  obj_properties[PROP_NODE] =
    g_param_spec_pointer ("node",
			  "Gegl node",
			  "Set the node which this property view is for",
			  G_PARAM_READWRITE);

  g_object_class_install_properties (gobject_class,
                                     N_PROPERTIES,
                                     obj_properties);

  g_signal_new("property-changed",
	       GEGL_GTK_TYPE_PROPERTY_VIEW,
	       G_SIGNAL_RUN_FIRST,
	       0, //no class method
	       NULL, //no accumulator,
	       NULL,
	       NULL,
	       G_TYPE_NONE,
	       0);

  g_signal_new("proxy-changed",
	       GEGL_GTK_TYPE_PROPERTY_VIEW,
	       G_SIGNAL_RUN_FIRST,
	       0, //no class method
	       NULL, //no accumulator,
	       NULL,
	       NULL,
	       G_TYPE_NONE,
	       3,
	       GEGL_TYPE_NODE,
	       G_TYPE_STRING,
	       G_TYPE_STRING,
	       G_TYPE_BOOLEAN); //node, property name, proxy text

  g_signal_new("query-proxy",
	       GEGL_GTK_TYPE_PROPERTY_VIEW,
	       G_SIGNAL_RUN_LAST,
	       0, //no class method
	       NULL, //no accumulator,
	       NULL,
	       NULL,
	       G_TYPE_STRING,
	       2,
	       GEGL_TYPE_NODE,
	       G_TYPE_STRING);
  //should connect to this and return a) the name of the parent property to which this property is connected
  //or b) NULL which means the property is not a connected to by a proxy
}

static void
gegl_gtk_property_view_init (GeglGtkPropertyView *self)
{
  gtk_orientable_set_orientation (GTK_ORIENTABLE (self),
                                  GTK_ORIENTATION_VERTICAL);
}

static void
gegl_gtk_property_view_dispose (GObject *object)
{
  GeglGtkPropertyView *self = (GeglGtkPropertyView *)object;

  G_OBJECT_CLASS (gegl_gtk_property_view_parent_class)->dispose (object);
}

static void
gegl_gtk_property_view_finalize (GObject *object)
{
  GeglGtkPropertyView *self = (GeglGtkPropertyView *)object;

  g_signal_handlers_destroy (object);
  G_OBJECT_CLASS (gegl_gtk_property_view_parent_class)->finalize (object);
}

GtkWidget *
gegl_gtk_property_view_new (GeglNode *node)
{
  return  (g_object_new (GEGL_GTK_TYPE_PROPERTY_VIEW,
			 "spacing",     0,
			 "homogeneous", FALSE,
			 "node", node,
			 NULL));
}

void
gegl_gtk_property_view_set_node (GeglGtkPropertyView *props, GeglNode *node)
{
  GValue val = G_VALUE_INIT;
  g_value_init(&val, G_TYPE_POINTER);
  g_value_set_pointer(&val, node);
  g_object_set_property(G_OBJECT(props), "node", &val);
}

typedef struct _propertycallbackdata
{
  GeglNode *node;
  GType prop_gtype;
  const gchar* prop_name;
  GeglGtkPropertyView *props;
  GtkWidget *value_box;
  GtkWidget *value_entry;
} PropertyCallbackData;

static
void property_changed(GtkWidget *widget, gpointer user_data)
{
  PropertyCallbackData *data = user_data;

  if(GTK_IS_ENTRY(widget)) {
    const gchar *text;
    text = gtk_entry_get_text(GTK_ENTRY(widget));

    gint	i_value;
    gdouble	d_value;

    switch(data->prop_gtype)
      {
      case G_TYPE_INT:
	i_value = (gint)strtod(text, NULL);
	gegl_node_set(data->node, data->prop_name, i_value, NULL);
	break;
      case G_TYPE_DOUBLE:
	d_value = (gdouble)strtod(text, NULL);
	gegl_node_set(data->node, data->prop_name, d_value, NULL);
	break;
      case G_TYPE_STRING:
	gegl_node_set(data->node, data->prop_name, text, NULL);
	break;
      default:
	g_print("Unknown property type: %s (%s)\n", data->prop_name, g_type_name(data->prop_gtype));
      }
  }
  else if(GTK_IS_COLOR_BUTTON(widget)) {
    GtkColorButton *button = GTK_COLOR_BUTTON(widget);

    GdkColor* sel_color = malloc(sizeof(GdkColor));
    gtk_color_button_get_color(button, sel_color);

    GeglColor* color = gegl_color_new(NULL);
    gegl_color_set_rgba(color, (double)sel_color->red/65535.0, 
			(double)sel_color->green/65535.0, 
			(double)sel_color->blue/65535.0,
			(double)gtk_color_button_get_alpha(button)/65535.0);

    free(sel_color);

    gegl_node_set(data->node, data->prop_name, color, NULL);
  }
  else if(GTK_IS_FILE_CHOOSER_BUTTON(widget)) {
    const gchar *file = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(widget));
    gegl_node_set(data->node, data->prop_name, file, NULL);
  }

  g_signal_emit_by_name(data->props, "property-changed");
}

static
void proxy_changed(GtkWidget *widget, gpointer user_data)
{
  PropertyCallbackData *data = user_data;

  const gchar *text = gtk_entry_get_text(GTK_ENTRY(widget));

  g_signal_emit_by_name(data->props, "proxy-changed", data->node, data->prop_name, text);
}

static gboolean
make_value_entry(GtkWidget *value_box, GType type, const gchar *name, GeglNode *node, int *d, PropertyCallbackData *data);

static void
proxy_checkbox_toggled (GtkToggleButton *togglebutton, PropertyCallbackData *data)
{
  gboolean active = gtk_toggle_button_get_active(togglebutton);
  if(active) {
    /*GtkWidget *combo = gtk_combo_new();
      GList *choices = NULL;
      choices = g_list_append(choices, "property1");
      choices = g_list_append(choices, "property2");
      choices = g_list_append(choices, "property3");

      gtk_combo_set_popdown_strings(GTK_COMBO(combo), choices);
      
      GList *list;
      if(list = gtk_container_get_children(GTK_CONTAINER(data->value_box)))
	gtk_container_remove(GTK_CONTAINER(data->value_box), GTK_WIDGET(list->data));

      gtk_box_pack_start(GTK_BOX(data->value_box), combo, TRUE, TRUE, 0);
      gtk_widget_show(combo);*/

    GList *list;
    if(list = gtk_container_get_children(GTK_CONTAINER(data->value_box)))
      gtk_container_remove(GTK_CONTAINER(data->value_box), GTK_WIDGET(list->data));

    GtkWidget*	value_entry = gtk_entry_new();
    gtk_entry_set_width_chars(GTK_ENTRY(value_entry), 2);
    g_signal_connect(value_entry, "activate", G_CALLBACK(proxy_changed), data);
    gtk_box_pack_start(GTK_BOX(data->value_box), value_entry, TRUE, TRUE, 0);
    gtk_widget_show(value_entry);
  }
  else {
    GList *list;
    if(list = gtk_container_get_children(GTK_CONTAINER(data->value_box)))
      gtk_container_remove(GTK_CONTAINER(data->value_box), GTK_WIDGET(list->data));
    make_value_entry(data->value_box, data->prop_gtype, data->prop_name, data->node, NULL, data);
    gtk_widget_show_all(data->value_box);
  }
}

//from http://developer.gnome.org/gtk/2.24/GtkFileChooser.html
static void
update_preview_cb (GtkFileChooser *file_chooser, gpointer data)
{
  GtkWidget *preview;
  char *filename;
  GdkPixbuf *pixbuf;
  gboolean have_preview;

  preview = GTK_WIDGET (data);
  filename = gtk_file_chooser_get_preview_filename (file_chooser);

  pixbuf = gdk_pixbuf_new_from_file_at_size (filename, 128, 128, NULL);
  have_preview = (pixbuf != NULL);
  g_free (filename);

  gtk_image_set_from_pixbuf (GTK_IMAGE (preview), pixbuf);
  if (pixbuf)
    g_object_unref (pixbuf);

  gtk_file_chooser_set_preview_widget_active (file_chooser, have_preview);
}

static void
file_browse (GtkButton *button, PropertyCallbackData *data)
{
  GtkWidget *window = gtk_widget_get_ancestor(GTK_WIDGET(button), GTK_TYPE_WINDOW);
  GtkWidget *preview = gtk_image_new ();
  GtkWidget *chooser = gtk_file_chooser_dialog_new("Select", GTK_WINDOW(window), 
						   GTK_FILE_CHOOSER_ACTION_SAVE, 
						   GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL, 
						   GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
						   NULL);

  gtk_file_chooser_set_filename(GTK_FILE_CHOOSER(chooser), gtk_entry_get_text(GTK_ENTRY(data->value_entry)));
  gtk_file_chooser_set_preview_widget (GTK_FILE_CHOOSER(chooser), preview);
  g_signal_connect(chooser, "update-preview",
            G_CALLBACK (update_preview_cb), preview);

  gint result = gtk_dialog_run(GTK_DIALOG(chooser));
  if(result == GTK_RESPONSE_ACCEPT)
    {
      gtk_entry_set_text(GTK_ENTRY(data->value_entry), gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(chooser)));
      g_signal_emit_by_name(data->value_entry, "activate");
    }

  gtk_widget_destroy(chooser);
}

static gboolean
make_value_entry(GtkWidget *value_box, GType type, const gchar *name, GeglNode *node, int *d, PropertyCallbackData *data)
{
  GtkWidget*	value_entry = gtk_entry_new();
  data->value_entry = value_entry;

  gchar buf[256] = "*";	//can probably be smaller; In fact, can probably do this without sprintf and a buffer. TODO: look at g_string

  gint		i_value;
  gdouble	d_value;
  gchar*	str_value;
  gboolean	skip = FALSE; //skip adding a generic text box in the case of special buttons for things like colors and filepaths

  switch(type)
    {
    case G_TYPE_INT:
      gegl_node_get(node, name, &i_value, NULL);
      sprintf(buf, "%d", i_value);
      break;
    case G_TYPE_DOUBLE:
      gegl_node_get(node, name, &d_value, NULL);
      sprintf(buf, "%.3f", d_value);
      break;
    case G_TYPE_STRING:
      gegl_node_get(node, name, &str_value, NULL);
      sprintf(buf, "%s", str_value);
      break;
    }

  if(type == GEGL_TYPE_BUFFER) {
    if(d)
      *d--;
    return FALSE;
  } else if( type == GEGL_TYPE_COLOR) {
    skip = TRUE;
    GtkWidget *color_button = gtk_color_button_new();//..._with_color(...);

    GeglColor* color;// = gegl_color_new(NULL);

    gegl_node_get(node, name, &color, NULL);

    gdouble r, g, b, a;
    gegl_color_get_rgba(color, &r, &g, &b, &a);

    GdkColor gdk_color;
    gdk_color.red = r*65535;
    gdk_color.green = g*65535;
    gdk_color.blue = b*65535;

    gtk_color_button_set_color(GTK_COLOR_BUTTON(color_button), &gdk_color);

    g_signal_connect(color_button, "color-set", (GCallback)property_changed, data);
    gtk_box_pack_start(GTK_BOX(value_box), color_button, TRUE, TRUE, 0);
	    
  } else if (!strcmp(name, "path")) { //create a special file browser for this property
    skip = TRUE;
    //GtkWidget *file_button = gtk_file_chooser_button_new("Select File", GTK_FILE_CHOOSER_ACTION_OPEN);
    //g_signal_connect(file_button, "file-set", (GCallback)property_changed, data);
    //gtk_box_pack_start(GTK_BOX(value_box), file_button, TRUE, TRUE, 0);

    gtk_entry_set_text(GTK_ENTRY(value_entry), buf);
    gtk_entry_set_width_chars(GTK_ENTRY(value_entry), 2);
    g_signal_connect(value_entry, "activate", G_CALLBACK(property_changed), data);

    GtkWidget *browse_button = gtk_button_new_with_label("Browse");
    g_signal_connect(browse_button, "clicked", G_CALLBACK(file_browse), data);

    gtk_box_pack_start(GTK_BOX(value_box), value_entry, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(value_box), browse_button, FALSE, FALSE, 0);
  }

  if(!skip)
    {
      gtk_entry_set_text(GTK_ENTRY(value_entry), buf);

      gtk_entry_set_width_chars(GTK_ENTRY(value_entry), 2);

      g_signal_connect(value_entry, "activate", G_CALLBACK(property_changed), data);

      gtk_box_pack_start(GTK_BOX(value_box), value_entry, TRUE, TRUE, 0);
    }

  return TRUE;
}

static
rebuild_contents(GeglGtkPropertyView *self)
{
  GeglNode *node = self->node;

  //empty
  GList *child;
  while(child = gtk_container_get_children(GTK_CONTAINER(self)))
    {
      gtk_container_remove(GTK_CONTAINER(self), GTK_WIDGET(child->data));
    }

  if(node && gegl_node_get_operation(node))
    {
      guint		n_props;
      GParamSpec**	properties = gegl_operation_list_properties(gegl_node_get_operation(node), &n_props);

      //TODO: only create enough columns for the properties which will actually be included (i.e. ignoring GeglBuffer props)
      GtkTable	*prop_table = GTK_TABLE(gtk_table_new(3, n_props, FALSE));
      gtk_box_pack_start(GTK_BOX(self), GTK_WIDGET(prop_table), TRUE, TRUE, 0);

      int d, i;
      for(d = 0, i = 0; i < n_props; i++, d++)
	{
	  GParamSpec* prop = properties[i];
	  GType	type = prop->value_type;
	  const gchar* name = prop->name;

	  GtkWidget* name_label = gtk_label_new(name);
	  gtk_misc_set_alignment(GTK_MISC(name_label), 0, 0.5);

	  GtkWidget *value_box = gtk_hbox_new(FALSE, 0);

	  PropertyCallbackData *data = g_new(PropertyCallbackData, 1);
	  data->node = node;
	  data->prop_name = name;
	  data->prop_gtype = type;
	  data->props = self;
	  data->value_box = value_box;

	  const gchar *proxy = NULL;
	  g_signal_emit_by_name(self, "query-proxy", node, name, &proxy);

	  if(proxy)
	    {
	      GtkWidget* value_entry = gtk_entry_new();
	      GString *str = g_string_new("<");
	      str = g_string_append(str, proxy);
	      str = g_string_append(str, ">");
	      gtk_entry_set_text(GTK_ENTRY(value_entry), str->str);
	      gtk_entry_set_width_chars(GTK_ENTRY(value_entry), 2);
	      g_signal_connect(value_entry, "activate", G_CALLBACK(proxy_changed), data);
	      gtk_box_pack_start(GTK_BOX(value_box), value_entry, TRUE, TRUE, 0);

	      gtk_table_attach(prop_table, name_label, 0, 1, d, d+1, GTK_FILL, GTK_FILL, 1, 1);
	      gtk_table_attach(prop_table, value_box, 1, 2, d, d+1, GTK_EXPAND | GTK_FILL | GTK_SHRINK, GTK_FILL, 1, 1);

	      /*GtkWidget *check = gtk_check_button_new();
	      gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(check), TRUE);
	      gtk_table_attach(prop_table, check, 2, 3, d, d+1, GTK_FILL, GTK_FILL, 1, 1);
	      g_signal_connect(check, "toggled", (GCallback)proxy_checkbox_toggled, data);*/
	    }
	  else if(make_value_entry(value_box, type, name, node, &d, data))
	    {
	      gtk_table_attach(prop_table, name_label, 0, 1, d, d+1, GTK_FILL, GTK_FILL, 1, 1);
	      gtk_table_attach(prop_table, value_box, 1, 2, d, d+1, GTK_EXPAND | GTK_FILL | GTK_SHRINK, GTK_FILL, 1, 1);
	      /*GtkWidget *check = gtk_check_button_new();
	      gtk_table_attach(prop_table, check, 2, 3, d, d+1, GTK_FILL, GTK_FILL, 1, 1);
	      g_signal_connect(check, "toggled", (GCallback)proxy_checkbox_toggled, data);*/
	    }
	}
  
      gtk_widget_show_all(GTK_WIDGET(prop_table));
    }
}
