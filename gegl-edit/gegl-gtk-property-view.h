#ifndef __GEGL_GTK_PROPERTY_VIEW_H__
#define __GEGL_GTK_PROPERTY_VIEW_H__

#include <gtk/gtk.h>


G_BEGIN_DECLS

#define GEGL_GTK_TYPE_PROPERTY_VIEW		\
  (gegl_gtk_property_view_get_type())
#define GEGL_GTK_PROPERTY_VIEW(obj)				\
  (G_TYPE_CHECK_INSTANCE_CAST ((obj),				\
			       GEGL_GTK_TYPE_PROPERTY_VIEW,	\
			       GeglGtkPropertyView))
#define GEGL_GTK_PROPERTY_VIEW_CLASS(klass)			\
  (G_TYPE_CHECK_CLASS_CAST ((klass),				\
			    GEGL_GTK_TYPE_PROPERTY_VIEW,	\
			    GeglGtkPropertyViewClass))
#define IS_GEGL_GTK_PROPERTY_VIEW(obj)				\
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj),				\
			       GEGL_GTK_TYPE_PROPERTY_VIEW))
#define IS_GEGL_GTK_PROPERTY_VIEW_CLASS(klass)			\
  (G_TYPE_CHECK_CLASS_TYPE ((klass),				\
			    GEGL_GTK_TYPE_PROPERTY_VIEW))
#define GEGL_GTK_PROPERTY_VIEW_GET_CLASS(obj)			\
  (G_TYPE_INSTANCE_GET_CLASS ((obj),				\
			      GEGL_GTK_TYPE_PROPERTY_VIEW,	\
			      GeglGtkPropertyViewClass))

typedef struct _GeglGtkPropertyView      GeglGtkPropertyView;
typedef struct _GeglGtkPropertyViewClass GeglGtkPropertyViewClass;
struct _GeglNode;
typedef struct _GeglNode GeglNode;

struct _GeglGtkPropertyViewClass
{
  GtkBoxClass parent_class;
};

struct _GeglGtkPropertyView
{
  GtkBox parent;

  GeglNode *node;

  GSList *callback_data; //free everything in this list when rebuilding view
};

GType gegl_gtk_property_view_get_type (void) G_GNUC_CONST;

GtkWidget* gegl_gtk_property_view_new (GeglNode *node);
void gegl_gtk_property_view_set_node (GeglGtkPropertyView *props, GeglNode *node); //NULL will clear

G_END_DECLS

#endif /* __GEGL_GTK_PROPERTY_VIEW_H__ */
